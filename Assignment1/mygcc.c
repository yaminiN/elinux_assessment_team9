#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>

int main(int argc, char *argv[])
{
	int f,s;
	char *p="/usr/bin/gcc";
	char *arg1=argv[1];
	char *arg2="-c";
	char *arg3="-o";
	char *arg4="hobj";
	char *arg5="hout";

	int f1,s1;
	
	if(argc<2)
	{
		printf("Please enter valid no of arguments (one .c file)");
		exit(1);
	}
	
	
	f=fork();

	if(f<0)
	{
		perror("error creating fork");
		exit(1);
	}

	if(f==0)
	{
		execl(p,p,arg2,arg1,arg3,arg4,NULL);
	}
	else{
		waitpid(-1,&s,0);


		if(s==0)
		{
			f1=fork();

			if(f1<0)
			{
				perror("error creating fork");
				exit(1);
			}

			if(f1==0)
			{
		//		printf("child2--\n");
				execl(p,p,arg1,arg3,arg5,NULL);
			}
			else
			{
		//		printf("child1 is a parent\n");
				waitpid(-1,&s1,0);
				//printf("%d",s1);
				if(s1==0)
				{
					printf("sucessfully compied %s\n",arg1);
					printf("Object file is hobj\n");
					printf("output file is hout\n");
				}
				else
					printf("error in linking\n");
			}
		}
		else
			printf("error in compiling");
	}

	return 0;
}
