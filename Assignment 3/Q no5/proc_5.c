#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/debugfs.h>
#include <linux/fs.h>
#include<linux/proc_fs.h>
#include<linux/sched.h>
#include<linux/sched/signal.h>
#include<linux/seq_file.h>
#include<linux/slab.h>
#include<linux/list.h>
#include<linux/random.h>
#define MAX_SIZE 4026

int n=1;
module_param(n,int,S_IRUGO);

static struct proc_dir_entry *pdir;
static struct proc_dir_entry *pentry;

struct dentry *debug_dir;
int filevalue;
int sum=0;
int count=0;
int len=MAX_SIZE;

typedef struct {
	int data;
	struct list_head lentry;
}data_list;
LIST_HEAD(prlist);

static int proc_show_demo(struct seq_file *m, void *v)
{
	struct list_head *curr;
	data_list *ptr;
	list_for_each(curr,&prlist)
	{
		ptr=list_entry(curr,data_list,lentry);
		seq_printf(m,"%d\n",ptr->data);
	}
	return 0; 
}

static int proc_open_demo(struct inode *inode, struct file *file)
{
	return single_open(file, proc_show_demo, NULL);
}
struct file_operations fops = { 
  .open=proc_open_demo,
  .release=single_release,
  .read = seq_read,
  .llseek=seq_lseek,
}; 

static int __init dfs_demo_init(void) {        //init_module

	int i=0;
	data_list *pnew;
	 debug_dir=debugfs_create_dir("psample", NULL);

 	 pdir=proc_mkdir("proctest",NULL);
	 pentry=proc_create("procsample",0444,pdir,&fops);
	proc_set_user(pentry,KUIDT_INIT(0),KGIDT_INIT(0));
	proc_set_size(pentry,80);
	
	
	for(i=0;i<n;i++)
	{
		pnew=kmalloc(sizeof( data_list),GFP_KERNEL);
		if(pnew == NULL) 
     		{
       			printk("Pseudo : kmalloc failed\n");
       			return -ENOMEM;             //goto end
    		 }

		pnew->data=i+100;
		sum+=pnew->data;
		count++;
		list_add(&pnew->lentry,&prlist);
	}
	debugfs_create_u32("sum",0444,debug_dir,&sum);
	debugfs_create_u32("number_of_list",0444,debug_dir,&count);

      	printk("Hello World..welcome\n");
  return 0;
}
static void __exit dfs_demo_exit(void) {       //cleanup_module
	data_list *ptr;
	struct list_head *prev,*pcur;
       
	list_for_each_safe(pcur,prev,&prlist)
	{
		ptr=list_entry(pcur,data_list,lentry);
		kfree(ptr);
	}
	debugfs_remove_recursive(debug_dir);
	remove_proc_entry("procsample",pdir);
	remove_proc_entry("proctest",NULL);
  	printk("Bye,Leaving the world\n");
}
module_init(dfs_demo_init);
module_exit(dfs_demo_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Your name");
MODULE_DESCRIPTION("A Hello, World Module");

