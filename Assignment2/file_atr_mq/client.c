#include<stdio.h>
#include<fcntl.h>
#include<mqueue.h>
#include<string.h>
#include<stdlib.h>
int main(int argc , char *argv[])
{
	int ret;

	mqd_t mqid;
	struct stat sat;
	int maxlen=8192,prio;

	if(argc<2)
	{
		printf("please Pass the valid No of arguments(1 valid file name");
		return 1;
	}
	mqid=mq_open("/mque1",O_RDWR|O_CREAT,0666,NULL);
	if(mqid<0)
	{
		perror("mqopen error:");
		exit(1);
	}
	char* str=argv[1];
	int len=strlen(str);
	
	ret=mq_send(mqid,str,len,5);
	if(ret<0)
	{
		perror("mq_send error:");
		exit(2);
	}
	mq_close(mqid);
	mqid=mq_open("/mque3",O_RDONLY|O_CREAT,0666,NULL);
	if(mqid<0)
	{
		perror("open_error:");
		exit(1);
	}
	printf("waiting for server reply\n");
	ret=mq_receive(mqid,(char*)&sat,maxlen,&prio);
	if(ret<0)
	{
		perror("error receieve");
		exit(2);
	}

	printf("Attributes Received from server\n");
	printf("******************************************\n");
	printf("\t\"%s\" attributes \n",str);
	printf("******************************************\n");
	printf("Device ID:%ld\n",sat.st_dev);
	printf("Inode No:%ld\n",sat.st_ino);
	printf("Protction mode:%d\n",sat.st_mode);
	printf("No of hard links:%ld\n",sat.st_nlink);
	printf("User Id of owner:%d\n",sat.st_uid);
	printf("group id of owner:%d\n",sat.st_gid);
	printf("Total sze in bytes:%ld\n",sat.st_size);
	printf("blocksize for filesystem I/O:%ld\n",sat.st_blksize);
	printf("number of blocks allocated:%ld\n",sat.st_blocks);
	printf("time of last access:%ld\n",sat.st_atime);
	printf("time of last modification :%ld\n",sat.st_mtime);
	printf("time of last status change:%ld\n",sat.st_ctime);
	mq_close(mqid);

	return 0;
}
