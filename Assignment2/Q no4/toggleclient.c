#include<mqueue.h>
#include<fcntl.h>
#include<stdio.h>

int main()
{
	int ret;
	char buf[8024];
	int maxlen=8192,prio,nbytes;
	mqd_t mqid,mqid1;
	mqid=mq_open("/mque7",O_RDWR|O_CREAT,0666,NULL);
	if(mqid<0)
	{
		perror("mq_open");
		exit(1);
	}
	char str[]="HELLO message";
	int len=strlen(str);
	printf("sending msg:%s\n",str);
	ret=mq_send(mqid,str,len,0);
	if(ret<0)
	{
		perror("mq_send");
		exit(2);
	}

	nbytes=mq_receive(mqid,buf,maxlen,&prio);
	if(nbytes <0)
	{
		perror("error");
		exit(2);
	}
	buf[nbytes]='\0';
	printf("received msg: %s\n",buf);
	mq_close(mqid);
	return 0;
}


