#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/debugfs.h>
#include <linux/fs.h>
#include<linux/sysfs.h>
#include<linux/sched.h>
#include<linux/sched/signal.h>
int ret;
#define MAX_SIZE 64
static struct kobject *example_kobj;
int count=0;
static ssize_t sample_show(struct kobject *kobj, struct kobj_attribute *attr,char *buf)
{
	struct task_struct *thread_task,*thread_temp;
	  printk("inside show--\n");
	count=0;
	for_each_process(thread_task)
	{
		for_each_thread(thread_task,thread_temp)	
		{
			count++;
		}

	}
  return sprintf(buf, "%d\n", count);
}
 
static struct kobj_attribute sample_attribute = __ATTR(fs_demo_attr,0660,sample_show,NULL);


static int __init dfs_demo_init(void) {        //init_module
	example_kobj =kobject_create_and_add("kobj_demo",kernel_kobj);
	ret=sysfs_create_file(example_kobj,&sample_attribute.attr);
      	printk("Hello World..welcome%d\n",count);
  return 0;
}
static void __exit dfs_demo_exit(void) {       //cleanup_module
	kobject_put(example_kobj);
  printk("Bye,Leaving the world\n");
}
module_init(dfs_demo_init);
module_exit(dfs_demo_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Your name");
MODULE_DESCRIPTION("A Hello, World Module");

