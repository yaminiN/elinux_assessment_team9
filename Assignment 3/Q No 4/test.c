#include <fcntl.h>
#include <unistd.h>
#include<stdio.h>
#include<stdlib.h>
#define MAX 1024
int main() {
  int fd,len, nbytes;
  fd = open("/proc/proctest/procsample", O_RDWR);
  if(fd<0) {
    perror("open");
    exit(1);
  }
  char rbuf[MAX];
  int maxlen = MAX;
  nbytes=read(fd, rbuf, maxlen);
  if(nbytes<0) {
    perror("read");
    exit(3);
  }

 
  rbuf[nbytes]='\0';
 printf("pid=%d,ppid=%d\n",getpid(),getppid());
  printf("%s\n",rbuf);
  close(fd);
 
  return 0;
}
