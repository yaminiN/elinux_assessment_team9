#include<stdio.h>
#include<mqueue.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>
#include<semaphore.h>

//void fun();

int main()
{
	int nbyte,f,s;
	int maxlen=8124,pr;

	char buf[8124],ch;
	int i,j;
	char *a[20];

	struct mq_attr attr;
	attr.mq_msgsize=512;
	attr.mq_maxmsg=10;

	mqd_t mqid;
	mqid=mq_open("/mque1",O_RDONLY|O_CREAT,0666,&attr);

	if(mqid<0)
	{
		perror("mq_open rror:");
		exit(1);
	}

	nbyte=mq_receive(mqid,buf,maxlen,&pr);
	if(nbyte<0)
	{
		perror("mq_receive:");
		exit(2);
	}
	buf[nbyte]='\0';
	printf("rcived:%s",buf);
	mq_close(mqid);


	f=fork();
	if(f<0)
	{
		perror("fork error");
		exit(1);
	}
	if(f==0)
	{
		

		printf("child--\n\n");
		execlp(buf,buf,NULL);	
	
	}
	else
	{
		waitpid(-1,&s,0);
		printf("parent--child staus:%d\n\n",s);

	}
	return 0;
}

