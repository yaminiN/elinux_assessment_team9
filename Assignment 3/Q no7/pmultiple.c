#include<pthread.h>
#include<stdio.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdlib.h>
#define MAXSIZE 100
void* task_body(void* pv)
{
	int i,id=(int)pv;
	printf("%d--welcome\n",id);
	for(i=1;i<=5;i++)
	{
		sleep(10);
	}
	//pthread_exit(NULL);
}
int main()
{
	int i,n=10,k,nbytes;
	char rbuf[64];
	pthread_t ptarr[n];
	int fd;
	fd=open("/sys/kernel/kobj_demo/fs_demo_attr",O_RDONLY,0666);
	if(fd==-1)
	{
		perror("error opening file\n");
		 exit(1);
	}

	for(i=0;i<n;i++)
	{
	  k=100+i;
	  pthread_create(&ptarr[i],NULL,task_body,(void*)k);
	}
	nbytes=read(fd, rbuf, MAXSIZE);
  	if(nbytes<0) {
    		perror("read");
    		exit(3);
 	 }

 	 rbuf[nbytes]='\0';
  	printf("nbytes=%d,rbuf=%s\n",nbytes,rbuf);
	
	for(i=0;i<n;i++)
	  pthread_join(ptarr[i],NULL);
	printf("main--thank you\n");
	close(fd);
	return 0;	//exit(0);
}

