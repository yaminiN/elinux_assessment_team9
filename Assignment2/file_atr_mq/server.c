#include<stdio.h>
#include<stdlib.h>

#include<string.h>

#include<fcntl.h>
#include<mqueue.h>
#include<sys/stat.h>

int main()
{
	mqd_t mqid1;
	struct mq_attr attr;
	attr.mq_msgsize=512;
	attr.mq_maxmsg=10;

	char buf[8192];
	int maxlen=8192,prio,nb;
	
	int status =0,ret,len,flag=0;
	struct stat satr;

	mqid1=mq_open("/mque1",O_RDWR|O_CREAT,0666,&attr);
	if(mqid1<0)
	{
		perror("mq_open error:");
		exit(1);
	}
	

	nb=mq_receive(mqid1,buf,maxlen,&prio);

	if(nb<0)
	{
		perror("mq_receive error:");
		exit(2);
	}
	buf[nb]='\0';
	printf("received: %s\n",buf);
	mq_close(mqid1);
	
	status =lstat(buf,&satr);
	if(status<0)
	{
		perror("lstat error:");
		flag=1;
		
	}

	mqid1=mq_open("/mque3",O_WRONLY|O_CREAT,0666,NULL);
	if(mqid1<0)
	{
		perror("mq_open rror:");
		exit(1);
	}
	
	len=sizeof(satr);
	
       	ret=mq_send(mqid1,(const char*)&satr,len,5);
	if(ret<0)
	{
		perror("send_error:");
		exit(2);
	}
	printf("Attributes sent successfully to client\n");
	mq_close(mqid1);


	return 0;
}
