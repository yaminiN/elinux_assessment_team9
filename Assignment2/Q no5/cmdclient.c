#include<stdio.h>
#include<mqueue.h>
#include<fcntl.h>
#include<string.h>
#include<stdlib.h>
int main()
{
	int ret,len;
	char cmd[100]="ls -a";
	mqd_t mqid;

	mqid= mq_open("/mque1",O_RDWR|O_CREAT,0666,NULL);

	if(mqid<0)
	{
		perror("Mq_open error:");
		exit(1);
	}

	len=strlen(cmd);
	ret=mq_send(mqid,cmd,len,5);

	if(ret<0)
	{
		perror("mq_sen error:");
		exit(2);
	}
	mq_close(mqid);

	return 0;
}
