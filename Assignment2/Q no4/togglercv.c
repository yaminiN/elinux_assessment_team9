#include<mqueue.h>
#include<fcntl.h>
#include<stdio.h>

int main()
{
	char ch;
	int ret,nbytes,c,len;
	struct mq_attr attr;
	attr.mq_msgsize=512;
	attr.mq_maxmsg=1;
	mqd_t mqid;
	mqid=mq_open("/mque7",O_RDWR|O_CREAT,0666,&attr);
	if(mqid<0)
	{
		perror("mq_open");
		exit(1);
	}
	char buf[8192];
	int maxlen=8192,prio;
	nbytes=mq_receive(mqid,buf,maxlen,&prio);
	if(nbytes<0)
	{
		perror("mq_recv");
		exit(2);
	}
	buf[nbytes]='\0';
	printf("received msg:%s\n",buf,nbytes,prio);
//	write(1,buf,nbytes);
	c=0;
	while(buf[c]!=0)
	{
		ch=buf[c];
		if(ch>='A' && ch<='Z')
			buf[c]+=32;
		else if(ch>='a' && ch<='z')
			buf[c]-=32;

		c++;
	}
	printf("sending msg: %s\n",buf);
	len=strlen(buf);
	ret=mq_send(mqid,buf,len,2);
	if(ret<0)
	{
		perror("error send");
		exit(2);
	}
	mq_close(mqid);
	return 0;
}


