#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>

#define MAX 20 //length os array
#define Thread_MAX 4 //you mention as many threads if required

int a[MAX]={23,45,12,34,67,87,89,90,9,98,88,76,54,32,21,13,25,36,47,58};
int min_th[Thread_MAX]={0};
int thread_no=0;

void* minimum(void* arg)
{
	int i,num = thread_no++;
	int min=10;
	for(i=num*(MAX / Thread_MAX);i<(num+1)*(MAX / Thread_MAX);i++)
	{
		if(a[i]<min)
			min=a[i];
	}
	min_th[num]=min;
	
}

int main()
{
	int i,min=10;
	pthread_t threads[Thread_MAX];
	for(i=0;i<Thread_MAX;i++)
		pthread_create(&threads[i],NULL,minimum,(void*)NULL);
	for(i=0;i<Thread_MAX;i++)
		pthread_join(threads[i],NULL);
	int totalsum=0;
	for(i=0;i<Thread_MAX;i++)
	{
		if(min_th[i]<min)
			min=min_th[i];
	}
	printf("minimum value is %d\n",min);
	return 0;
}

