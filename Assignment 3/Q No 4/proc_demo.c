#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/debugfs.h>
#include <linux/fs.h>
#include<linux/proc_fs.h>
#include<linux/sched.h>
#include<linux/seq_file.h>
#define MAX_SIZE 1024
static struct proc_dir_entry *pdir;
static struct proc_dir_entry *pentry;
int len=MAX_SIZE;
char pbuffer[MAX_SIZE];
static int proc_show_demo(struct seq_file *m, void *v)
{
	seq_printf(m, "pid: %i\n",current->pid);
	seq_printf(m,"parent pid: %i\n",task_ppid_nr(current));
	seq_printf(m,"process name: %s\n",current->comm);
	seq_printf(m,"cpu: %u\n",current->on_cpu);
	seq_printf(m,"priority: %d\n",current->prio);
	seq_printf(m,"scheduling policy: %d\n",current->policy);
	seq_printf(m,"uid: %hu,gid: %hu\n",current->cred->gid,current->cred->uid);
	seq_printf(m,"mjr flt: %lx,minr flt: %lx\n",current->maj_flt,current->min_flt);
	return 0;
}

static int proc_open_demo(struct inode *inode, struct file *file)
{
	return single_open(file, proc_show_demo, NULL);
}
/*ssize_t my_show(struct seq_file *m,void *p)
{
	seq_printf(m,"current pid=%d\n",current->pid);
	return 0;
}	
int pdemo_open(struct inode* inode , struct file* file)
{
	
   return single_open(file,my_show,NULL);


} */
struct file_operations fops = { 
  .open=proc_open_demo,
  .release=single_release,
  .read = seq_read,
  .llseek=seq_lseek,
}; 

 

static int __init dfs_demo_init(void) {        //init_module
pdir=proc_mkdir("proctest",NULL);
pentry=proc_create("procsample",0666,pdir,&fops);
proc_set_user(pentry,KUIDT_INIT(0),KGIDT_INIT(0));
proc_set_size(pentry,80);
      	printk("Hello World..welcome\n");
  return 0;
}
static void __exit dfs_demo_exit(void) {       //cleanup_module
  remove_proc_entry("procsample",pdir);
  remove_proc_entry("proctest",NULL);
	printk("Bye,Leaving the world\n");
}
module_init(dfs_demo_init);
module_exit(dfs_demo_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Your name");
MODULE_DESCRIPTION("A Hello, World Module");

